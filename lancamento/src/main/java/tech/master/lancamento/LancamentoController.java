package tech.master.lancamento;

import javax.validation.Valid;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class LancamentoController {

	@Autowired
	private LancamentoService lancamentoService;

	private Lancamento lancamento;

	@Autowired
	private KafkaTemplate<String, String> template;
	
	private ObjectMapper mapper;

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Lancamento criar(@Valid @RequestBody Lancamento lancamento) throws Exception {
		this.lancamento = lancamentoService.criar(lancamento);
		
		if (this.lancamento != null) {
			mapper = new ObjectMapper();
			template.send(new ProducerRecord<String, String>("cartao", "1", "Criar Lancamento: " + mapper.writeValueAsString(this.lancamento)));
		}
		
		return this.lancamento;
	}

	@GetMapping("{numero}")
	public Iterable<Lancamento> buscar(@PathVariable String numero) {
		return lancamentoService.buscarTodasPorNumeroDoCartao(numero);
	}
}
