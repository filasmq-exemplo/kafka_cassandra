package tech.master.lancamento;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", fallback=CartaoFallBack.class)
public interface CartaoClient {
	@GetMapping("/buscar/{numero}")
	public Optional<Cartao> buscar(@PathVariable("numero") String numero);
}
