package tech.master.lancamento;

import org.springframework.data.repository.CrudRepository;

public interface LancamentoRepository extends CrudRepository<Lancamento, Integer> {
	Iterable<Lancamento> findAllByNumero(String numero);
}
