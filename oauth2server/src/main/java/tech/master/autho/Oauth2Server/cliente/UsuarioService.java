package tech.master.autho.Oauth2Server.cliente;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {
  @Autowired
  private BCryptPasswordEncoder encoder;

  @Autowired
  private UsuarioRepository repository;
  
  public Usuario criar(Usuario usuario) {
    usuario.setSenha(encoder.encode(usuario.getSenha()));
    return repository.save(usuario);
  }

  @Override
  public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
    Optional<Usuario> optional = repository.findByCpf(cpf);

    if (!optional.isPresent()) {
      throw new UsernameNotFoundException("Usuário não encontrado");
    }

    Usuario usuario = optional.get();

    SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");

    return new User(usuario.getCpf(), usuario.getSenha(), Collections.singletonList(authority));
  }
}
