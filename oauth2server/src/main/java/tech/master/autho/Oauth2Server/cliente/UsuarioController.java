package tech.master.autho.Oauth2Server.cliente;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
  @Autowired
  private UsuarioService service;
  
  @GetMapping("/me")
  public Map<String, String> validar(Principal principal) {
    HashMap<String, String> sensValues = new HashMap<String, String>();
    sensValues.put("name", principal.getName());
    return sensValues;
  }
  
  @PostMapping("/usuario")
  public Usuario criar(@RequestBody @Valid Usuario usuario) {
    return service.criar(usuario);
  }
}
