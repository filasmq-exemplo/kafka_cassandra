package tech.master.cartao;

import java.security.Principal;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class CartaoController {

	@Autowired
	private CartaoService cartaoService;

	@Autowired
	private KafkaTemplate<String, String> template;


	private ObjectMapper mapper;
	private Cartao cartao;

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cartao criar(@Valid @RequestBody Cartao cartao, Principal principal) throws Exception {
		cartao.setCpfCliente(principal.getName());
		this.cartao = cartaoService.criar(cartao);

		if (this.cartao != null) {
			mapper = new ObjectMapper();
			template.send(new ProducerRecord<String, String>("topiconaoehdasuaconta", "Cartao: Criou " + mapper.writeValueAsString(this.cartao)));
		}

		return this.cartao;
	}

	@PatchMapping("/{numero}/ativar")
	public void ativar(@PathVariable String numero) throws Exception  {
		Optional<Cartao> optional = cartaoService.buscar(numero);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		cartaoService.ativar(numero);
		
		if (this.cartao != null) {
			mapper = new ObjectMapper();
			template.send(new ProducerRecord<String, String>("topiconaoehdasuaconta", "Cartao: Ativou " + mapper.writeValueAsString(this.cartao)));
		}
	}

	@GetMapping("/buscar/{numero}")
	public Optional<Cartao> buscar(@PathVariable String numero) {
		Optional<Cartao> optional = cartaoService.buscar(numero);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return optional;
	}

}
