package tech.master.cartao;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class CartaoService {
  private CartaoRepository cartaoRepository;

  private ClienteClient clienteClient;

  @Autowired
  public CartaoService(CartaoRepository cartaoRepository, ClienteClient clienteClient) {
    super();
    this.cartaoRepository = cartaoRepository;
    this.clienteClient = clienteClient;
  }

  public Cartao criar(Cartao cartao) {

    Optional<Cliente> cliente = clienteClient.buscar(cartao.getCpfCliente());

    if (!cliente.isPresent()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    cartao.setNumero(gerarNumero());
    cartao.setAtivo(false);

    return cartaoRepository.save(cartao);
  }

  public Optional<Cartao> buscar(String numero) {
    return cartaoRepository.findById(numero);
  }

  private String gerarNumero() {
    Random random = new Random();
    String numero = "";

    for (int i = 0; i < 16; i++) {
      numero += random.nextInt(9);
    }

    if (cartaoRepository.findById(numero).isPresent()) {
      return gerarNumero();
    }

    return numero;
  }

  public void ativar(String numero) {
    Optional<Cartao> cartaoOptional = cartaoRepository.findById(numero);

    if (!cartaoOptional.isPresent()) {
      /* throw new ValidacaoException("numero", "Cartão não encontrado."); */
      throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }

    Cartao cartao = cartaoOptional.get();

    if (cartao.getAtivo()) {
      /* throw new ValidacaoException("numero", "Cartão já está ativo."); */
      throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }

    cartao.setAtivo(true);
    cartaoRepository.save(cartao);
  }

}
