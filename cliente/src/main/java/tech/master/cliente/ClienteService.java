package tech.master.cliente;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
  @Autowired
  private ClienteRepository clienteRepository;
  @Autowired
  private UsuarioClient usuarioClient;
  
  public Cliente criar(Usuario usuario) {
    usuarioClient.criar(usuario);
    
    Cliente cliente = new Cliente();
    cliente.setNome(usuario.getNome());
    cliente.setCpf(usuario.getCpf());
    
    return clienteRepository.save(cliente);
  }
  
  public Optional<Cliente> buscar(int id) {
    return clienteRepository.findById(id);
  }
  
  public Optional<Cliente> buscarPorCpf(String cpf) {
    return clienteRepository.findByCpf(cpf);
  }
}
