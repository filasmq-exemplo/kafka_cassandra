package tech.master.cliente;

import javax.validation.constraints.NotBlank;

public class Usuario {
  @NotBlank
  private String nome;
  @NotBlank
  private String cpf;
  @NotBlank
  private String senha;
  
  public String getCpf() {
    return cpf;
  }
  
  public String getSenha() {
    return senha;
  }
  
  public String getNome() {
    return nome;
  }
  
  public void setCpf(String cpf) {
    this.cpf = cpf;
  }
  
  public void setSenha(String senha) {
    this.senha = senha;
  }
  
  public void setNome(String nome) {
    this.nome = nome;
  }
}
