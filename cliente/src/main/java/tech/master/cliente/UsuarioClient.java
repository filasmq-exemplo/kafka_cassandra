package tech.master.cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "authserver")
public interface UsuarioClient {

  @PostMapping("/usuario")
  public Usuario criar(Usuario usuario);
}
