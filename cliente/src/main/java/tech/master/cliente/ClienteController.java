package tech.master.cliente;

import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

//import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ClienteController {
	@Autowired
	private ClienteService clienteService;
	
	private Cliente cliente;

	@Autowired
	private KafkaTemplate<String, String> template;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
//	private ObjectMapper mapper;

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cliente criar(@Valid @RequestBody Usuario usuario) throws Exception {
		logger.info("Criando usuário " + usuario.getCpf());
		cliente = clienteService.criar(usuario);
		
		if(cliente != null) {
//			mapper = new ObjectMapper();
			template.send(new ProducerRecord<String, String>("topiconaoehdasuaconta",
					"Cliente: Criou o cliente " + cliente.getNome() + " com sucesso"));
		}
		
		return cliente;
	}

	@GetMapping("/buscar/{cpf}")
	public Optional<Cliente> buscar(@PathVariable String cpf) {
		Optional<Cliente> cliente = clienteService.buscarPorCpf(cpf);

		if (cliente.isPresent()) {
			return cliente;
		}

		return Optional.empty();
	}
}