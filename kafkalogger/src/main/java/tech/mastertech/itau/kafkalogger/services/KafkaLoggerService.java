package tech.mastertech.itau.kafkalogger.services;

import java.io.IOException;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

//import com.fasterxml.jackson.core.JsonParseException;
//import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.kafkalogger.models.LogAplicacao;
import tech.mastertech.itau.kafkalogger.respositories.KafkaLoggerRepository;

@Component
public class KafkaLoggerService {
	
//	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private KafkaLoggerRepository kafkaLoggerRepository;
	
	@KafkaListener(id="brenoconsumer", topics="topiconaoehdasuaconta")
	public void log(@Payload String str){
		LogAplicacao log = new LogAplicacao();
		log.setDataHora(LocalDate.now());
		log.setDescricao(str);
		
		kafkaLoggerRepository.save(log);
		System.out.println(str);
	}
	
}
