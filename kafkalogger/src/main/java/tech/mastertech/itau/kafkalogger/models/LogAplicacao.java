package tech.mastertech.itau.kafkalogger.models;

import java.time.LocalDate;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class LogAplicacao {
	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private LocalDate dataHora;

	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private String descricao;

	public LocalDate getDataHora() {
		return dataHora;
	}

	public void setDataHora(LocalDate dataHora) {
		this.dataHora = dataHora;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}