package tech.mastertech.itau.kafkalogger.respositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.kafkalogger.models.LogAplicacao;

public interface KafkaLoggerRepository extends CrudRepository<LogAplicacao, UUID>{

}
