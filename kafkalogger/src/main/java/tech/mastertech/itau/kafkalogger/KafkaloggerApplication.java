package tech.mastertech.itau.kafkalogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaloggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaloggerApplication.class, args);
	}

}
